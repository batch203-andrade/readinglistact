function add(arrNum = [], num = prompt('How many numbers to add? Please input below: ')) {
    for (let i = 1; i <= num; i++) {
        arrNum.push(parseInt(prompt(`Enter Number ${i}:`)));
    }

    let sum = 0;
    for (let i = 0; i < arrNum.length; i++) {
        sum += arrNum[i];
    }

    alert(`The total of the entered ${num} numbers is ${sum}`);

    return arrNum;
}

let storedArray = add();

console.log(storedArray);


function findDuplicate(arr) {
    let check = {};
    let duplicates = [];

    for (let i = 0; i < arr.length; i++) {
        check[arr[i]] && duplicates.push(arr[i]);
        if (check[arr[i]] === undefined) {
            check[arr[i]] = 1;
        }
    }
    alert(`Duplicates fruits found: [${duplicates}]`);
    return duplicates;
}

let fruits = ['apple', 'grapes', 'orange', 'mango', 'apple', 'strawberry', 'grapes'];

console.log(findDuplicate(fruits));

let numbers = [2, 4, 6, 8, 10];
let cubes = numbers.map(e => e ** 3);
for (let i = 0; i < cubes.length; i++) {
    console.log("The cube value of " + numbers[i] + " is " + cubes[i]);
}

